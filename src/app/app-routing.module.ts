import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderbookComponent } from './orderbook/orderbook.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: OrderbookComponent,
    data: {
      title: 'Coinvine'
    },
    children: [
      {
        path: 'order-book',
        loadChildren: () => import('./orderbook/orderbook.module').then(m => m.OrderbookModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
